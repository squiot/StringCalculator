package com.company;

import com.company.exception.NegativeValueException;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {

    private Pattern patternNumeric = Pattern.compile("-?\\d+(\\.\\d+)?");
    private Pattern patternCustomDelimiter = Pattern.compile("(//)(.)");
    public String reg_delimiter  = ",|\n";
    private final int LIMIT = 1000;

    public StringCalculator(){

    }

    public boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        return patternNumeric.matcher(strNum).matches();
    }

    public int add(String input) throws NegativeValueException {

        Matcher m = patternCustomDelimiter.matcher(input);
        if(m.find()){
                reg_delimiter+="|" + m.group(2);
        }

        List<String> values = List.of(input.split(reg_delimiter));
        List<String> neg_values = new ArrayList<>();
        int sum = 0;
        for (String v : values) {
            if(isNumeric(v)){
                if(isNegative(v)){
                    neg_values.add(v);
                }else if(!isUnderValidThreshold(v,LIMIT)){
                    sum+=Integer.parseInt(v);
                }
            }
        }

        if(!neg_values.isEmpty()){
            String message = "";
            if(neg_values.size()>1){
                message += String.join(",", neg_values);
            }
            throw new NegativeValueException(message);
        }

        return sum;
    }


    public boolean isUnderValidThreshold(String value,int limit){
        return Integer.parseInt(value)>limit;
    }

    public boolean isNegative(String value){
        return Integer.parseInt(value)<0;
    }

}
