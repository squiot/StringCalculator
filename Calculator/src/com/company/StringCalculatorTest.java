package com.company;

import com.company.exception.NegativeValueException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    private StringCalculator sc;

    @BeforeEach
    public void setUp() {
        sc = new StringCalculator();
    }

    @Test
    void addEmpty() throws NegativeValueException {
        assertEquals(0,sc.add(""));
    }

    @Test
    void addOneValue() throws NegativeValueException {
        String value = "3";
        assertEquals(3,sc.add(value));
    }

    @Test
    void addTwoValues() throws NegativeValueException {
        String value = "3,2";
        assertEquals(5,sc.add(value));
    }

    @Test
    void addSeveralValues() throws NegativeValueException {
        String value = "10,39,1,20,2,92";
        assertEquals(164,sc.add(value));
    }

    @Test
    void addSeveralValuesButSomeWrongValues() throws NegativeValueException {
        String value = "10,,abc,20,10a,11";
        assertEquals(41,sc.add(value));
    }

    @Test
    void valueIsNumeric(){
        String value = "101";
        Assert.assertTrue(sc.isNumeric(value));
    }

    @Test
    void valueIsNotNumeric(){
        String value = "abs";
        Assert.assertFalse(sc.isNumeric(value));
    }

    @Test
    void negativeValueIsNumeric(){
        String value = "-101";
        Assert.assertTrue(sc.isNumeric(value));
    }

    @Test
    void valueIsNotValid(){
        String value = "10abs-";
        Assert.assertFalse(sc.isNumeric(value));
    }

    @Test
    void emptyValueIsIgnored(){
        String value = "";
        Assert.assertFalse(sc.isNumeric(value));
    }

    @Test
    void addWithEscapeDelimiter() throws NegativeValueException {
        String value = "1\n2,5";
        Assert.assertEquals(8,sc.add(value));
    }

    @Test
    void valueIsOver1000(){
        String value = "1039";
        Assert.assertTrue(sc.isUnderValidThreshold(value,1000));
    }

    @Test
    void addIgnoreValueOver1000() throws NegativeValueException {
        String value = "1,30,1002,40";
        Assert.assertEquals(71,sc.add(value));
    }

    @Test
    void addWithAnyDelimiter() throws NegativeValueException {
        String value = "//;\n1,2;3";
        Assert.assertEquals(6,sc.add(value));

    }

    @Test
    void addWithNegativeValues() {
        String value = "1,-30,1002,40";
        assertThrows(NegativeValueException.class,
                ()->{
                    sc.add(value);
                });
    }
}