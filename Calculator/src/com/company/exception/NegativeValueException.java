package com.company.exception;

public class NegativeValueException extends Exception{

    public NegativeValueException(){
        super();
    }

    public NegativeValueException(String message){
        super("Les nombres négatifs ne sont pas autorisés " + message);
    }
}
